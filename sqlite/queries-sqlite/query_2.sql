CREATE VIEW const_subquery2 AS
  SELECT col2, col4 FROM (SELECT src AS col2, trg AS col4 FROM isLocatedIn) AS t;
CREATE VIEW fixpoint_relation1_X1 AS 
WITH RECURSIVE fixpoint_relation1_X1 (y, col4) AS(
    SELECT y, col4 FROM (SELECT y, col4 FROM (SELECT src AS y, trg AS col4 FROM isLocatedIn) AS t) AS const
  UNION 
    SELECT fixpoint_relation1_X1.y, const_subquery2.col4 FROM fixpoint_relation1_X1 JOIN const_subquery2 ON fixpoint_relation1_X1.col4 = const_subquery2.col2
)
SELECT DISTINCT * FROM fixpoint_relation1_X1
;
CREATE VIEW const_subquery4 AS
  SELECT col3, trg FROM (SELECT src AS col3, trg FROM dealsWith) AS t;
CREATE VIEW fixpoint_relation3_X2 AS 
WITH RECURSIVE fixpoint_relation3_X2 (col4, trg) AS (
    SELECT col4, trg FROM (SELECT col4, trg FROM (SELECT src AS col4, trg FROM dealsWith) AS t) AS const
  UNION 
    SELECT fixpoint_relation3_X2.col4, const_subquery4.trg FROM fixpoint_relation3_X2 JOIN const_subquery4 ON fixpoint_relation3_X2.trg = const_subquery4.col3
)
SELECT DISTINCT * FROM fixpoint_relation3_X2
;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, y FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT    src AS x, trg AS col1 FROM hasChild) AS t) AS t1 NATURAL JOIN (SELECT col1, y FROM (SELECT    src AS col1, trg AS y FROM livesIn) AS t) AS t2) AS t) AS t1 NATURAL JOIN (SELECT y FROM (SELECT * FROM (SELECT y, trg FROM (SELECT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation3_X2) AS t2) AS t) AS t WHERE trg = '1930288') AS t) AS t2) AS t) AS t;
