# Efficient Enumeration of Recursive Plans in Transformation-based Query Optimizers

This repository contains the datasets, queries and instructions needed for reproducing experiments with the Recursive Logical Query Directed Acyclic Graph (RLQDAG) introduced in the following paper:

Efficient Enumeration of Recursive Plans in Transformation-based Query Optimizers. Amela Fejza, Pierre Genevès, Nabil Layaïda. VLDB 2024 [arXiv:2312.02572 [cs.DB]](https://arxiv.org/abs/2312.02572).

### Datasets

- [Yago](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/yago): a knowledge graph containing 62,643,951 edges, 42,832,856 nodes and 83 predicates. 
- [Bahamas Leaks](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/bahamas-leaks): a property graph that provides names of directors and owners of Bahamian companies, trusts and foundations registered between 1990 and 2016, together with their connections. It contains 202,242 nodes and 249,190 edges.
- [Airbnb](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/airbnb): a property graph with 24,840 nodes and 14000 edges.
- [LDBC social network](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/ldbc): a property graph with 908,224 nodes and 1,960,654 edges.
- [Wikitree](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/wikitree): a tree with 1,382,751 nodes and 9,192,212 edges.
- [Academic tree](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/academic-tree): a tree with 765,655 nodes and 1,561,493 edges. 



### Queries

- [Yago queries](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/queries/yago_queries.csv).
- [Bahamas Leaks queries](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/queries/bahamas-leaks_queries.csv).
- [Airbnb queries](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/queries/airbnb_queries.csv).
- [LDBC queries](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/queries/ldbc_queries.csv).



## Instructions for running the compared systems



### Engines used for comparison are:



- [MilleniumDB](#milleniumdb)
- [Virtuoso](#virtuoso)
- [Neo4j](#neo4j)
- [Blazegraph](#blazegraph) 
- [PostgreSQL](#postgresql)
- [MuEnum](#muenum)
- [RLQDAG](#rlqdag)
- [MySQL](#mysql)
- [SQLite](#sqlite)

# MilleniumDB

In order to install MilleniumDB, you need to clone the gitlab [repository](https://github.com/MillenniumDB/MillenniumDB) and please follow the instructions in the `README.md` to compile the project.

- The [file](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/milleniumdb/yago.txt) for the Yago database creation.

- The [queries](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/milleniumdb/yago-queries-milleniumdb.csv)
written in the MilleniumDB query language.

Create the Yago database using the following command: `build/Release/bin/create_db outputyago.txt [path/to/new_database_folder]`

The server can be started with this command: `build/Release/bin/server [path/to/database_folder]`

A query can be run using this command: `build/Release/bin/query [path/to/query_file]`

It is recommended to write each one of the queries found in this [file](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/milleniumdb/yago-queries-milleniumdb.csv) as explained above in order to observe the time used for query parser/optimizer.

It is recommended to clear the cache using this command after running each query `sync; echo 3 > /proc/sys/vm/drop_caches` and restart the server before running each one of the queries.

# Virtuoso

Virtuoso can be downloaded from the github [link](https://github.com/openlink/virtuoso-opensource/releases).
The experiments reported in the paper use Virtuoso version 7.2.6.1.

You can download Virtuoso with this command: `wget https://github.com/openlink/virtuoso-opensource/releases/download/v7.2.6.1/virtuoso-opensource.x86_64-generic_glibc25-linux-gnu.tar.gz` 

Then extract with this command: `tar -xf virtuoso-opensource.x86_64-generic_glibc25-linux-gnu.tar.gz` 

and then enter to the folder `virtuoso-opensource` .

This .nt [file](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main//virtuoso/virtuoso-format.nt) needed for virtuoso to create the database for Yago is provided.

The configuration file has to be edited in order to create the Yago database in Virtuoso. At first, run this command `cp database/virtuoso.ini.sample yago.ini`.

Then, `yago.ini` file is edited.
- replace every `../database/` with the path of the database folder you want to create.

- add the path of folder where you have `[virtuoso_nt_file]` and the path of the database folder you want to create to `DirsAllowed`.

- change `VADInstallDir` to the path of `virtuoso-opensource/vad`.

- set `NumberOfBuffers` to `5450000`.

- set `MaxDirtyBuffers` to `4000000`.

- set `ResultSetMaxRows` to `300000` (no query produces more results).

- set `MaxQueryExecutionTime` to `600`. It is equal to 10 minutes.

- add at the end of the file these lines:

  ```
  [Flags]
  tn_max_memory = 2755359740
  ```
- add the full path to [Plugins] section

In order to load the data you can start the server as follows:
- `bin/virtuoso-t -c yago.ini +foreground`

Leave this running and open another terminal to run the following command: `bin/isql localhost:1111`. Inside this console you should run the following commands:

- `ld_dir('[path_to_folder_where_nt_file_is]', '[virtuoso_nt_file]', 'database_name');`
- `rdf_loader_run();`

After the data is loaded it is recommended to run `checkpoint;` to be sure the database is saved even after stopping the server.

Once the database is created you can run the [queries](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/irtuoso/yago-queries-virtuoso.csv) inside the `isql` console (p.s. please add `;` at the end of each query).

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.




# Neo4j

Neo4j can be downloaded from the [website](https://neo4j.com/download-center/#community). 
The experiments reported in the paper use Neo4j version 4.4.11.

After the file is downloaded, it can be extracted by running the following command: `tar -xf neo4j-community-4.4.11-unix.tar.gz` .
Then enter to the folder  `cd neo4j-community-4.4.11/` and set the variable `$NEO4J_HOME` that points to Neo4J folder by using `export` and adding it to .bashrc/.zshrc .

After the first steps are successfully completed you can edit the configuration file that can be found in `conf/neo4j.conf`.
The changes needed are:
- Set `dbms.default_database=yago_neo4j`
- Uncomment the line `dbms.security.auth_enabled=false`
- Add the line `dbms.transaction.timeout=10m`

The files needed for database creation:
- [yagofacts_with_id_neo4j.csv](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/neo4j-files/yagofacts_with_id_neo4j.csv) 
- [yagoNodesNeo4j.csv](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/neo4j-files/yagoNodesNeo4j.csv)

The database is created by running the following command:
```
bin/neo4j-admin import --database yago_neo4j \
--relationships yagofacts_with_id_neo4j.csv \ 
--nodes=Entity=yagoNodesNeo4j.csv \
--delimiter "," --array-delimiter ";"
```


Once the database is created, the index for entities needs to be created as well. <br/>
In order to do this, at first the server is started by using the following command: `bin/neo4j console`.<br/>
In another terminal, open the cypher console by running the following command: `bin/cypher-shell`, and then
run this comand `CREATE INDEX ON :Entity(id);`. <br/>
After it finishes, use this command to check if the indexes are properly created `SHOW INDEXES;` .

In order to run the queries you need the following files:
- [queries_file](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/neo4j-files/yago-queries-neo4j.txt)
- [neo4j_paths.py](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/neo4j-files/neo4j_paths.py)

And then you can run the following command:
`python neo4j_property_paths.py [queries_file] 300000 name`

The results can be found in the path written in `RESUME_FILE` inside the `neo4j_property_paths.py` file.

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.

# Blazegraph
For Blazegraph configuration some instructions are inspired and adapted from the following [repository](https://github.com/MillenniumDB/benchmark/tree/master). 
The experiments reported in the paper use Blazegraph version 2.1.6.

Before installing Blazegraph the following prerequisites need to be installed:
- Java JDK (with `$JAVA_HOME` defined and `$JAVA_HOME/bin` on `$PATH`)
- Maven
- Git

For Debian/Ubuntu based Linux distributions you can run the following commands:
- `sudo apt update`
- `sudo apt install openjdk-11-jdk mvn git`

The .nt [file](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/blazegraph/blazegraph-format.nt) is correctly parsed to be used by Blazegraph configuration.

To help the data loading it is recommended to split .nt into smaller files. 
You can run the following commands:
- `mkdir splitted_yago_nt`
- `cd splitted_yago_nt`
- `split -l 1000000 -a 4 -d --additional-suffix=.nt [yago_nt_file]`
- `cd ..`


To use Blazegraph you will need the following:
- `git clone --recurse-submodules https://gerrit.wikimedia.org/r/wikidata/query/rdf wikidata-query-rdf`
- `cd wikidata-query-rdf`
- `mvn package`
- `cd dist/target`
- `tar xvzf service-*-dist.tar.gz`
- `cd service-*/`
- `mkdir logs`

The following lines of the default script file `runBlazegraph.sh`  should be updated like this:
- the line of the main memory: `HEAP_SIZE=${HEAP_SIZE:-"Xg"}` should be updated with the value corresponding to how much RAM your machine has
- in the line that corresponds to the log folder `LOG_DIR=${LOG_DIR:-"/path/to/logs"}`, you need to replace `/path/to/logs` with the absolute path of the `logs` dir that was created in the previous step.
- add `-Dorg.wikidata.query.rdf.tool.rdf.RdfRepository.timeout=600` to the `exec java` command to specify the timeout (value is in seconds).
- change `-Dcom.bigdata.rdf.sparql.ast.QueryHints.analyticMaxMemoryPerQuery=0` which removes per-query memory limits.
- add the line  `-Dcom.bigdata.rdf.store.DataLoader.closure=None` to not get an IO Exception during data loading.

To load the data , at first you need to start the server in a terminal using the following command  `./runBlazegraph.sh`.
Then, in another terminal start the import with the following command:  `./loadRestAPI.sh -n wdq -d [path_of_splitted_nt_folder]`.

In order to execute the queries run the following command:
`python blazegraph_paths.py [queries_file] 300000 name`


- The file of yago querie for blazegraph can be found in [yago-queries-blazegraph.txt](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/blazegraph/yago-queries-blazegraph.txt)
- The file used to run the queries is [blazegraph_paths.py](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/blazegraph/blazegraph_paths.py).
- `name` is an extension for the file name put after `300000`

Edit the parameter `ROOT` in the file `blazegraph_paths.py` and the results will be found in the path `ROOT/out/[BLAZEGRAPH_300000_name]`. 

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`. 


# PostgreSQL

The data for the Yago dataset in each csv (and in all queries over this dataset) are encoded with integers, and if you want to find what integer corresponds to a value in yagofacts you can check the file [encoding_yagofacts.txt](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/datasets/encoding_yagofacts.txt). 

At first you need to download PostgreSQL from the [website](https://www.postgresql.org/).
The experiments reported in the paper use PostgreSQL version 15.1.
In order for psql command to work, make sure that you put the correct path to it based on the installation.
The following settings are used for the PostgreSQL database.
```
Host: localhost
Port: 5432
User: postgres
Password: postgres
Database: postgres
```
Then you need to create the database and the tables needed using Yago dataset.
For the database creation and table creation with the content of Yago dataset you can use [create_db_postgres.sh](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/postgres/create_db_postgres.sh). 
For this script to work you need to make sure to be placed in the directory where psql command can work, this can change based on your installation. 

Make sure to set the file permissions to allow execution:
```
chmod +x create_db_postgres.sh
```

Then you can run the following: 
```
./create_db_postgres.sh /path/to/your/csv_folder
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/yago) that contains the csv files for the Yago dataset.

For these experiments, the statement timeout is set to 600000 ms.<br/>
To connect to the database you can execute the following: `psql -U <user>`, where `<user>` is `postgres` in this case.<br/>
Then, to change the statement timeout run the following command:
```
begin transaction;
set statement_timeout = 600000; 
commit;
```

The SQL queries run over the Yago dataset for PostgreSQL are in this [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/postgres/queries-postgres-vanilla).
You can execute the queries in PostgreSQL using [execute_queries_postgres.sh](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/postgres/execute_queries_postgres.sh).

Make sure to set the file permissions to allow execution: 
```
chmod +x execute_queries_postgres.sh
```
Then you can run the following: 
```
./execute_queries_postgres.sh /path/to/your/csv_folder
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/postgres/queries-postgres-vanilla) that contains the Yago queries written in SQL. After this script is executed, for each one of the queries (in the same folder) one file named `result_{i}` (where i is the query number) will be created. This file contains the evaluation time in milliseconds.

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.

# MuEnum
The SQL queries generated by MuEnum are shown in this [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/postgres/queries-postgres-muenum). 
To run the queries for MuEnum with PostgreSQL as backend the same scripts are used. 
You can run the following: 
```
./execute_queries_postgres.sh /path/to/your/csv_folder
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/postgres/queries-postgres-muenum) that contains the Yago queries chosen after optimization with MuEnum written in SQL. After this script is executed, for each one of the queries (in the same folder) one file named `result_{i}` (where i is the query number) will be created. This file contains the evaluation time in milliseconds.

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.
# RLQDAG
The SQL queries generated by the RLQDAG system are shown in this [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/postgres/queries-postgres-rlqdag). 
To run the RLQDAG-generated queries with PostgreSQL as backend the same scripts are used.
You can run the following: 
```
./execute_queries_postgres.sh /path/to/your/csv_folder
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/postgres/queries-postgres-rlqdag) that contains the Yago queries chosen after optimization with RLQDAG written in SQL. After this script is executed, for each one of the queries (in the same folder) one file named `result_{i}` (where i is the query number) will be created. This file contains the evaluation time in milliseconds.

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.

# MySQL
At first you need to download MySQL from the [website](https://www.mysql.com/fr/).
The experiments reported in the paper use MySQL version 8.1.0.
In order for mysql command to work, make sure that you put the correct path to it based on the installation.
The following settings are used for the MySQL database.
```
DB_NAME="yago"
MYSQL_USER="root"
MYSQL_PASSWORD="pass12"
HOST="localhost"
PORT="3306"
```
Then you need to create the database and the tables needed for the Yago dataset.
For the database creation and table creation with the content of Yago dataset you can use [create_db_mysql.sh](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/mysql/create_db_mysql.sh). For this script to work you need to make sure to be placed in the directory where mysql command can work, this can change based on your installation. 

Make sure to set the file permissions to allow execution: 
```
chmod +x create_db_mysql.sh
```

Then you can run the following:
```
./create_db_mysql.sh /path/to/your/csv_folder
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/yago) that contains the csv files for the Yago dataset.

The sql version of queries run over the Yago dataset for PostgreSQL can be found in this [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/mysql/queries-mysql).
You can execute the queries in PostgreSQL using [execute_queries_mysql.sh](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/mysql/execute_queries_mysql.sh).

Make sure to set the file permissions to allow execution: 
```
chmod +x execute_queries_mysql.sh
```
Then you can run the following: 
```
./execute_queries_mysql.sh /path/to/your/csv_folder
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/mysql/queries-mysql) that contains the Yago queries written in sql. After this script is executed, for each one of the queries (in the same folder) one file named `result_{i}` (where i is the query number) will be created. This file contains the evaluation time in milliseconds.

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.

# SQLite

At first you need to download SQLite from the [website](https://www.sqlite.org/index.html).
The experiments reported in the paper use SQLite version 3.36.0.
In order for sqlite3 command to work, make sure that you put the correct path to it based on the installation.

Then you need to create the database and the tables needed for the Yago dataset.
For the database creation and table creation with the content of Yago dataset you can use [create_db_sqlite.sh](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/sqlite/create_db_sqlite.sh). For this script to work you need to make sure to be placed in the directory where sqlite3 command can work, this can change based on your installation. 

Make sure to set the file permissions to allow execution: 
```
chmod +x create_db_sqlite.sh
```

Then you can run the following: 
```
./create_db_sqlite.sh /path/to/your/csv_folder /absolute/path/to/database/yago.db
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/datasets/yago) that contains the csv files for the Yago dataset; and `/absolute/path/to/database/yago.db` is the absolute path where the database is created, named yago.db in this case.

The sql version of queries run over the Yago dataset for PostgreSQL can be found in this [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/sqlite/queries-sqlite).
You can execute the queries in PostgreSQL using [execute_queries_sqlite.sh](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/sqlite/execute_queries_sqlite.sh).

Make sure to set the file permissions to allow execution:
```
chmod +x execute_queries_sqlite.sh
```
Then you can run the following: 
```
./execute_queries_sqlite.sh /path/to/your/csv_folder /absolute/path/to/database/yago.db
```
where `/path/to/your/csv_folder` is the [folder](https://gitlab.inria.fr/tyrex-public/rlqdag/-/tree/main/sqlite/queries-sqlite) that contains the Yago queries written in sql; and `/absolute/path/to/database/yago.db` is the absolute path to Yago database. After this script is executed, for each one of the queries (in the same folder) one file named `result_{i}` (where i is the query number) will be created. This file contains the evaluation time in milliseconds.

It is recommended to clear the cache using this command after running the queries `sync; echo 3 > /proc/sys/vm/drop_caches`.



# Specific parameters and system versions used
All [charts](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/paper_charts) presented in the paper can be reproduced with the following settings:
- The time budget for plan space exploration is set by default to 20 milliseconds when using RLQDAG unless specified otherwise in the paper.
- Using MilleniumDB from this [repository](https://github.com/MillenniumDB/MillenniumDB) as described above.
- Using Virtuoso version 7.2.6.1 and adding `NumberOfBuffers` to `5450000`, `MaxDirtyBuffers` to `4000000`, `ResultSetMaxRows` to `300000`,`MaxQueryExecutionTime` to `600`.
- Using Neo4j version 4.4.11. Notice that Q9 is not supported in Neo4j, because forms like (a/b)+ cannot be expressed in Cypher.
- Using Blazegraph version 2.1.6 and adding `HEAP_SIZE` to `192g` and `-Dorg.wikidata.query.rdf.tool.rdf.RdfRepository.timeout=600`.
- Using PostgreSQL version 15.1.
- Using MySQL version 8.1.0.
- Using SQLite version 3.36.0.

