CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT y, col1 FROM (SELECT id, src AS y, trg AS col1 FROM islocatedin) AS t);
CREATE TEMPORARY VIEW const_subquery3 AS
  (SELECT col2, trg FROM (SELECT id, src AS col2, trg FROM dealswith) AS t);
DO $$BEGIN
CREATE TEMPORARY TABLE fixpoint_tmp AS
  (SELECT y, trg FROM (SELECT y, trg FROM (SELECT * FROM (SELECT y, col3 FROM (SELECT id, src AS y, trg AS col3 FROM islocatedin) AS t) AS t1 NATURAL JOIN (SELECT col3, trg FROM (SELECT id, src AS col3, trg FROM dealswith) AS t) AS t2) AS t) AS t);
CREATE TEMPORARY TABLE fixpoint_relation1_X1 AS (SELECT * FROM fixpoint_tmp);
WHILE EXISTS (SELECT 1 FROM fixpoint_relation1_X1) LOOP
  CREATE TEMPORARY TABLE nouvelles AS
    (SELECT y, trg FROM (SELECT trg, y FROM (SELECT trg, y FROM (SELECT * FROM (SELECT y AS col1, trg FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS t1 UNION SELECT trg, y FROM (SELECT y, trg FROM (SELECT * FROM (SELECT y, trg AS col2 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery3) AS t) AS t2) AS rec_req EXCEPT SELECT * FROM fixpoint_tmp);
  INSERT INTO fixpoint_tmp (SELECT * FROM nouvelles);
  DROP TABLE fixpoint_relation1_X1;
  ALTER TABLE nouvelles RENAME TO fixpoint_relation1_X1;
END LOOP;
DROP TABLE fixpoint_relation1_X1;
ALTER TABLE fixpoint_tmp RENAME TO fixpoint_relation1_X1;
END;$$;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, y FROM (SELECT id, src AS x, trg AS y FROM livesin) AS t) AS t1 NATURAL JOIN (SELECT y FROM (SELECT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t WHERE trg = '57917422') AS t) AS t2) AS t) AS t;