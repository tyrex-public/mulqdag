CREATE TEMPORARY VIEW const_subquery3 AS
  (SELECT col2, trg FROM (SELECT id, src AS col2, trg FROM dealswith) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation2_X2 (col3, trg) AS
    SELECT col3, trg FROM (SELECT col3, trg FROM (SELECT id, src AS col3, trg FROM dealswith) AS t) AS const
  UNION 
    SELECT col3, trg FROM (SELECT trg, col3 FROM (SELECT * FROM (SELECT col3, trg AS col2 FROM fixpoint_relation2_X2) AS t NATURAL JOIN const_subquery3) AS t) AS rec;
CREATE TEMPORARY VIEW const_subquery4 AS
  (SELECT y, col1 FROM (SELECT id, src AS y, trg AS col1 FROM islocatedin) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (y) AS
    SELECT y FROM (SELECT y FROM (SELECT trg, y FROM (SELECT * FROM (SELECT * FROM (SELECT * FROM fixpoint_relation2_X2) AS t WHERE trg = '57917422') AS t1 NATURAL JOIN (SELECT y, col3 FROM (SELECT id, src AS y, trg AS col3 FROM islocatedin) AS t) AS t2) AS t) AS t) AS const
  UNION 
    SELECT y FROM (SELECT y FROM (SELECT * FROM (SELECT y AS col1 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery4) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, y FROM (SELECT id, src AS x, trg AS y FROM livesin) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation1_X1) AS t2) AS t) AS t;