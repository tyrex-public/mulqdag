#!/bin/bash

# Variables
DB_NAME="postgres"
CSV_FOLDER="$1"
HOST="localhost"
PORT="5432"
USER="postgres"
PASSWORD="postgres"

# Check if the database already exists
db_exists=$(PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -lqt | cut -d \| -f 1 | grep -wq "$DB_NAME" && echo "true" || echo "false")

# If database doesn't exist, create it
if [ "$db_exists" == "false" ]; then
    PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -c "CREATE DATABASE $DB_NAME;"
    echo "Database $DB_NAME created."
else
    echo "Database $DB_NAME already exists."
fi

# Get all CSV files in the folder
csv_files=("$CSV_FOLDER"/*.csv)

# Loop through each CSV file and import into the database
for file in "${csv_files[@]}"
do
    table_name=$(basename "$file" .csv) # Extract table name from the CSV file name
    
    
    # Create table
    PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -d "$DB_NAME" -c "CREATE TABLE $table_name (id varchar, src varchar, trg varchar);"
    
    # Import CSV data into the table
    PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -d "$DB_NAME" -c "\\copy $table_name FROM '$file' DELIMITER ',' CSV HEADER;"
    
    # Add indexes for each column
    id_index_name="id_index_${table_name}"
    src_index_name="src_index_${table_name}"
    trg_index_name="trg_index_${table_name}"
    PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -d "$DB_NAME" -c "CREATE INDEX $id_index_name ON $table_name (id);"
    PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -d "$DB_NAME" -c "CREATE INDEX $src_index_name ON $table_name (src);"
    PGPASSWORD="$PASSWORD" psql -h "$HOST" -p "$PORT" -U "$USER" -d "$DB_NAME" -c "CREATE INDEX $trg_index_name ON $table_name (trg);"
    
    echo "Imported data from $file into table $table_name and added indexes for columns."
done

echo "All CSV files imported into the database $DB_NAME successfully."
