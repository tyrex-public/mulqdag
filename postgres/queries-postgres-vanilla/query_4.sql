CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT col1, col4 FROM (SELECT id, src AS col1, trg AS col4 FROM islocatedin) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (col2, col4) AS
    SELECT col2, col4 FROM (SELECT col2, col4 FROM (SELECT id, src AS col2, trg AS col4 FROM islocatedin) AS t) AS const
  UNION 
    SELECT col2, col4 FROM (SELECT col4, col2 FROM (SELECT * FROM (SELECT col2, col4 AS col1 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS rec;
CREATE TEMPORARY VIEW const_subquery4 AS
  (SELECT col3, trg FROM (SELECT id, src AS col3, trg FROM dealswith) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation3_X2 (col4, trg) AS
    SELECT col4, trg FROM (SELECT col4, trg FROM (SELECT id, src AS col4, trg FROM dealswith) AS t) AS const
  UNION 
    SELECT col4, trg FROM (SELECT trg, col4 FROM (SELECT * FROM (SELECT col4, trg AS col3 FROM fixpoint_relation3_X2) AS t NATURAL JOIN const_subquery4) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, trg FROM (SELECT * FROM (SELECT x, col4 FROM (SELECT * FROM (SELECT x, col2 FROM (SELECT id, src AS x, trg AS col2 FROM livesin) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation1_X1) AS t2) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation3_X2) AS t2) AS t) AS t WHERE trg = '57917422') AS t) AS t;