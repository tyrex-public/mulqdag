CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT col1, col3 FROM (SELECT id, src AS col1, trg AS col3 FROM islocatedin) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (x, col3) AS
    SELECT x, col3 FROM (SELECT x, col3 FROM (SELECT id, src AS x, trg AS col3 FROM islocatedin) AS t) AS const
  UNION 
    SELECT x, col3 FROM (SELECT col3, x FROM (SELECT * FROM (SELECT x, col3 AS col1 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS rec;
CREATE TEMPORARY VIEW const_subquery4 AS
  (SELECT col2, trg FROM (SELECT id, src AS col2, trg FROM dealswith) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation3_X2 (col3, trg) AS
    SELECT col3, trg FROM (SELECT col3, trg FROM (SELECT id, src AS col3, trg FROM dealswith) AS t) AS const
  UNION 
    SELECT col3, trg FROM (SELECT trg, col3 FROM (SELECT * FROM (SELECT col3, trg AS col2 FROM fixpoint_relation3_X2) AS t NATURAL JOIN const_subquery4) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, trg FROM (SELECT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation3_X2) AS t2) AS t) AS t WHERE trg = '57917422') AS t) AS t;