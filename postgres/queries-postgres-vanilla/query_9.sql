CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT col2, trg FROM (SELECT * FROM (SELECT col2, col1 FROM (SELECT id, src AS col2, trg AS col1 FROM actedin) AS t) AS t1 NATURAL JOIN (SELECT trg, col1 FROM (SELECT id, src AS trg, trg AS col1 FROM actedin) AS t) AS t2) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (x, trg) AS
    SELECT x, trg FROM (SELECT x, trg FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT id, src AS x, trg AS col1 FROM actedin) AS t) AS t1 NATURAL JOIN (SELECT trg, col1 FROM (SELECT id, src AS trg, trg AS col1 FROM actedin) AS t) AS t2) AS t) AS const
  UNION 
    SELECT x, trg FROM (SELECT trg, x FROM (SELECT * FROM (SELECT x, trg AS col2 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t WHERE trg = '99436162') AS t) AS t;