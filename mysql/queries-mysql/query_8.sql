CREATE   VIEW const_subquery2 AS
  (SELECT col1, col3 FROM (SELECT    src AS col1, trg AS col3 FROM isLocatedIn) AS t);
CREATE VIEW fixpoint_relation1_X1 AS (
WITH RECURSIVE fixpoint_relation1_X1 (x, col3) AS(
    SELECT x, col3 FROM (SELECT x, col3 FROM (SELECT src AS x, trg AS col3 FROM isLocatedIn) AS t) AS const
  UNION 
    SELECT fixpoint_relation1_X1.x, const_subquery2.col3 FROM fixpoint_relation1_X1 JOIN const_subquery2 ON fixpoint_relation1_X1.col3 = const_subquery2.col1
)
SELECT DISTINCT * FROM fixpoint_relation1_X1
); 
CREATE   VIEW const_subquery4 AS
  (SELECT col2, trg FROM (SELECT    src AS col2, trg FROM dealsWith) AS t);
CREATE VIEW fixpoint_relation3_X2 AS (
WITH RECURSIVE fixpoint_relation3_X2 (col3, trg) AS (
    SELECT col3, trg FROM (SELECT col3, trg FROM (SELECT src AS col3, trg FROM dealsWith) AS t) AS const
  UNION 
    SELECT fixpoint_relation3_X2.col3, const_subquery4.trg FROM fixpoint_relation3_X2 JOIN const_subquery4 ON fixpoint_relation3_X2.trg = const_subquery4.col2
)
SELECT DISTINCT * FROM fixpoint_relation3_X2
);
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, trg FROM (SELECT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation3_X2) AS t2) AS t) AS t WHERE trg = '57917422') AS t) AS t;