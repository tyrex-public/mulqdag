#!/bin/bash

# Directory containing the SQL query files
QUERIES_DIR="$1"

# MySQL connection details
DB_NAME="yago"
MYSQL_USER="root"
MYSQL_PASSWORD="pass12"
HOST="localhost"
PORT="3306"

# Loop through query files from query_1 to query_9
for ((i = 1; i <= 9; i++)); do
    QUERY_FILE="${QUERIES_DIR}/query_${i}.sql"

    # Check if the query file exists
    if [ -f "$QUERY_FILE" ]; then
        # Execute the query and measure execution time
        start=$(date +%s%N)
        mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" < "$QUERY_FILE" > /dev/null
        end=$(date +%s%N)

        # Calculate execution time in milliseconds
        execution_time=$((($end - $start) / 1000000))
        
        # Get all views from the database
        views=$(mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "USE $DB_NAME; SHOW FULL TABLES WHERE TABLE_TYPE = 'VIEW';" | awk '!/Tables_in_/ {print $1}')

        # Loop through each view and drop it
        for view in $views
        do
            mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "DROP VIEW $view;"
            echo "Dropped view: $view"
        done
        
        # Save execution time to result file
        echo "Query ${i} Execution Time (ms): ${execution_time}" > "${QUERIES_DIR}/result_${i}"
    else
        echo "Query file query_${i} not found."
    fi
done
