from SPARQLWrapper import SPARQLWrapper, JSON
from socket import timeout
import time
import subprocess
import traceback
import os
import re
import sys
print(1)
# This file is inspired and adapted from gitlab repository: https://github.com/MillenniumDB/benchmark

# Usage:
# python benchmark_property_paths.py <ENGINE> <QUERIES_FILE_ABSOLUTE_PATH> <LIMIT> <SUFFIX_NAME>
# LIMIT = 0 will not add a limit



if len(sys.argv) < 4:
    print("usage:\npython file.py <path_of_queries> <results_limit> <name>")
    sys.exit()


QUERIES_FILE = sys.argv[1]
LIMIT        = sys.argv[2]
SUFFIX_NAME  = sys.argv[3]

############################ EDIT THESE PARAMETERS #############################
TIMEOUT = 600 # Max time per query in seconds
ROOT = 'root'
################################################################################

# It is suggested to use absolute paths to avoid problems with current directory
ENGINE_PATH = f'{ROOT}/blazegraph/service'

PORT = 9999

ENDPOINT = 'http://localhost:9999/bigdata/namespace/wdq/sparql'

SERVER_CMD =  './runBlazegraph.sh'

# Path to needed output and input files

RESUME_FILE      = f'{ROOT}/out/paths_BLAZEGRAPH_limit_{LIMIT}_{SUFFIX_NAME}.csv'
ERROR_FILE       = f'{ROOT}/out/errors/paths_BLAZEGRAPH_limit_{LIMIT}_{SUFFIX_NAME}.log'

SERVER_LOG_FILE  = f'{ROOT}/out/log/paths_BLAZEGRAPH_limit_{LIMIT}_{SUFFIX_NAME}.log'


# create output folders if they do not exist
if not os.path.exists(f'{ROOT}/out/log/'):
    os.makedirs(f'{ROOT}/out/log/')

if not os.path.exists(f'{ROOT}/out/errors/'):
    os.makedirs(f'{ROOT}/out/errors/')

server_log = open(SERVER_LOG_FILE, 'w')
server_process = None

# Check if output file already exists
if os.path.exists(RESUME_FILE):
    print(f'File {RESUME_FILE} already exists. Remove it or choose another prefix.')
    sys.exit()


def lsof(pid):
    process = subprocess.Popen(['lsof', '-a', f'-p{pid}', f'-i:{PORT}', '-t'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = process.communicate()
    return out.decode('UTF-8').rstrip()

def lsofany():
    process = subprocess.Popen(['lsof', '-t', f'-i:{PORT}'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = process.communicate()
    return out.decode('UTF-8').rstrip()


# Parse query
def parse_to_sparql(query):
    if not LIMIT:
        return f'SELECT DISTINCT ?x WHERE {{ {query} }}'
    return f'SELECT DISTINCT ?x WHERE {{ {query} }} LIMIT {LIMIT}'


def start_server():
    global server_process
    print('starting server...')

    server_log.write("[start server]\n")
    server_process = subprocess.Popen(SERVER_CMD, stdout=server_log, stderr=server_log, cwd=ENGINE_PATH)
    print(f'pid: {server_process.pid}')

    while not lsof(server_process.pid):
        time.sleep(1)

    print(f'done')


def kill_server():
    global server_process
    print(f'killing server[{server_process.pid}]...')
    server_log.write("[kill server]\n")
    server_process.kill()
    server_process.wait()

    while lsof(server_process.pid):
        time.sleep(1)

    print('done')


# Send query to server
def execute_queries():
    with open(QUERIES_FILE) as queries_file:
        for line in queries_file:
            query_number, query = line.split(',')
            print(f'Executing query {query_number}')
            query_sparql(query, query_number)
                


def query_sparql(query_pattern, query_number):
    query = parse_to_sparql(query_pattern)
    #query = test()
    count = 0
    sparql_wrapper = SPARQLWrapper(ENDPOINT)
    sparql_wrapper.setTimeout(TIMEOUT+10)
    sparql_wrapper.setReturnFormat(JSON)
    sparql_wrapper.setQuery(query)

    start_time = time.time()

    try:
        # Compute query
        results = sparql_wrapper.query()
        json_results = results.convert()
        for r in json_results["results"]["bindings"]:
            count += 1
            
        elapsed_time = int((time.time() - start_time) * 1000) # Truncate to miliseconds

        with open(RESUME_FILE, 'a') as file:
            file.write(f'{query_number},{count},OK,{elapsed_time}\n')

    except timeout as e:
        elapsed_time = int((time.time() - start_time) * 1000) # Truncate to miliseconds
        with open(RESUME_FILE, 'a') as file:
            file.write(f'{query_number},{count},TIMEOUT,{elapsed_time}\n')

        with open(ERROR_FILE, 'a') as file:
            file.write(f'Exception in query {str(query_number)}: {str(e)}\n')

        kill_server()
        start_server()

    except Exception as e:
        elapsed_time = int((time.time() - start_time) * 1000) # Truncate to miliseconds
        with open(RESUME_FILE, 'a') as file:
            file.write(f'{query_number},{count},ERROR({type(e).__name__}),{elapsed_time}\n')

        with open(ERROR_FILE, 'a') as file:
            file.write(f'Exception in query {str(query_number)} [{type(e).__name__}]: {str(e)}\n')

        kill_server()
        start_server()


with open(RESUME_FILE, 'w') as file:
    file.write('query_number,results,status,time\n')

with open(ERROR_FILE, 'w') as file:
    file.write('')

if lsofany():
    raise Exception("other server already running")


start_server()
execute_queries()

if server_process is not None:
    kill_server()

server_log.close()
