from neo4j import GraphDatabase
import time
import sys
import os

# This file is inspired and adapted from gitlab repository: https://github.com/MillenniumDB/benchmark

QUERIES_FILE = sys.argv[1]
LIMIT        = sys.argv[2]

############################ EDIT THESE PARAMETERS #############################
ROOT = 'root'
RESUME_FILE = f'{ROOT}/property_paths_NEO4J_limit_{LIMIT}.txt'

BOLT_ADDRESS = 'bolt://127.0.0.1:7687'
DATABASE_NAME = 'yago_neo4j'
###############################################################################

driver = GraphDatabase.driver(BOLT_ADDRESS)

with driver.session(database=DATABASE_NAME) as session, open(QUERIES_FILE, 'r') as queries_file:
    for line in queries_file:
        query_number, query = line.strip().split(',')
        result_count = 0
        start_time = time.time()
        try:
            print("executing query", query_number)
            if LIMIT != 0:
                query += f' LIMIT {LIMIT}'
            results = session.read_transaction(lambda tx: tx.run(query).data())
            for _ in results:
                result_count += 1

            elapsed_time = int((time.time() - start_time) * 1000) # Truncate to miliseconds
            with open(RESUME_FILE, 'a') as output_file:
                output_file.write(f'{query_number},{result_count},OK,{elapsed_time}\n')

        except Exception as e:
            elapsed_time = int((time.time() - start_time) * 1000) # Truncate to miliseconds
            with open(RESUME_FILE, 'a') as output_file:
                output_file.write(f'{query_number},{result_count},ERROR({type(e).__name__}),{elapsed_time}\n')
            print(e)

